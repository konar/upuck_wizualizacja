function[bluetooth_object] = create_bluetooth_object(device_name)
    b = instrhwinfo('Bluetooth', device_name);
    bluetooth_object = eval(b.ObjectConstructorName{1});
end
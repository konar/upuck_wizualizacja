clear all; close all;

%Bluetooth device name
device_name = 'HC-05';

%map resolution
res = 0.0001;

%ring size along X axis
x_ring_size = 0.7;

%ring size along Y axis
y_ring_size = 1;

%base size along X axis
x_base_size = 0.21;

%base size along Y axis
y_base_size = 0.3;

%arrow length
l = 0.2;

%frame length
frame_length = 13;

bt = create_bluetooth_object(device_name);

'Opening connection'
fopen(bt);
flushinput(bt);

fh = figure;

frame = repmat(uint8(0),1,frame_length);

i = 1;

set(gcf, 'CurrentCharacter','!');
while double(get(gcf,'CurrentCharacter'))~=27
    frame(i) = fread(bt,1);
    i = i + 1;
    
    if frame(1)~='!'
        i = 1;
    elseif i==(frame_length+1)
        'Frame received'
        x = typecast(frame(2:5), 'SINGLE');
        y = typecast(frame(6:9), 'SINGLE');
        theta = typecast(frame(10:13), 'SINGLE');
        i = 1;
        draw_ring(fh,res,x_ring_size,y_ring_size,x_base_size,y_base_size,l,x,y,theta);
        drawnow;
    end
end

fclose(bt);
clear bt;
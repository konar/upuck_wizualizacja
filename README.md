# README #

To repozytorium zawiera kod źródłowy wizualizacji danych zebranych przez małego robota "uPuck".

Jest to studencki projekt małego robota autonomicznego, którego celem jest udział w kokurencji "Puck Collect". Robot wziął udział w zawodach Robomaticon 2016 w Warszawie, gdzie zajął pierwsze miejsce (patrz plik dyplom).
Robot jest wyposażony w:
 * czujniki IR do wykrywania przeszkód;
 * czujnik IR do wykrywania zebranych krążków;
 * 2 czujniki RGB po I2C - do ustalenia koloru bazy i zebranego krążka;
 * 2 silniki DC i enkodery kwadraturowe do nich;
 * moduł Bluetooth po UART do komunikacji z laptopem.
 
Kod źródłowy samego robota znajduje się w tym [repozytorium](https://bitbucket.org/konar/upuck_program).
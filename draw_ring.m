function[] = draw_ring(figure_handler, map_resolution, x_ring_size, y_ring_size, x_base_size, y_base_size, arrow_length, x_robot, y_robot, theta_robot)

set(0, 'currentfigure', figure_handler);
hold off;

x=0:map_resolution:x_base_size;
y1=zeros(1,length(x));
y2=ones(1,length(x))*y_base_size;
X=[x,fliplr(x)];
Y=[y1,fliplr(y2)];
fill(X,Y,'b');

hold on;

x=(x_ring_size-x_base_size):map_resolution:x_ring_size;
y1=ones(1,length(x))*y_ring_size;
y2=ones(1,length(x))*(y_ring_size-y_base_size);
X=[x,fliplr(x)];
Y=[y1,fliplr(y2)];
fill(X,Y,'r');

u = arrow_length*cos(theta_robot);
v = arrow_length*sin(theta_robot);
quiver(x_robot,y_robot,u,v, 0, 'g', 'MaxHeadSize', 20*arrow_length);

axis equal;
xlim([0,x_ring_size]);
ylim([0,y_ring_size]);
title('Press Esc to exit');
end